import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Storage } from '../core/storage';
import { marvelParams } from '../core/constants';

@Injectable()
export class BaseService {

hds:HttpHeaders;
constructor(protected http: HttpClient) {}

get( url: string ) : Observable<Object> { 
    url += marvelParams;
    return this.http.get(url);
}

post(url: string, body: any) {
    return this.http.post(url, body, this.httpOptions());
}

patch(url: string, body: any) {
    return this.http.patch(url, body, this.httpOptions());
}

put(url: string, body: any) {
    return this.http.put(url, body, this.httpOptions());
}

remove(url: string, type: number = 1) {
    let data = {"_method": "delete", "type": `${type}`};
    return this.http.post(url, data, this.httpOptions());
}

protected httpOptions() : {headers: HttpHeaders} {
    this.hds = new HttpHeaders({
        'Accept': 'application/json',
        'Authorization': `Bearer ${this.getToken()}`
    });
    return  { headers: this.hds};
}

getToken() {
    if (Storage.check('session')) {
        let session:any = Storage.getAll('session');
        return session.access_token;
    }
    return '';
}

}