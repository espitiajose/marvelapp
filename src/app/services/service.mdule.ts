import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CharacterService } from './character.service';
import { ComicService } from './comic.service';


@NgModule({
    imports: [
        CommonModule
    ],
    providers: [
        CharacterService,
        ComicService
    ],
  })
export class ServicesModule {}