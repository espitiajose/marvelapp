import { Injectable } from '@angular/core';
import { API_URL } from '../core/constants';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class CharacterService extends BaseService {

  urls = {
    characters : API_URL+"/characters"
  }

  constructor(http: HttpClient) {
    super(http);
  }

  getCharacters(offset:number = 0, sortBy?:string, name?:string, limit:number = 10){
    let url = this.urls.characters + "?limit="+limit + "&offset="+offset;
    if(sortBy) url += "&orderBy="+sortBy+'&';
    if(name) url += "nameStartsWith="+name+'&';
    return this.get(url);
  }

  getCharacter(id:string){
    let url = this.urls.characters + "/"+id+"?";
    return this.get(url);
  }
  
}
