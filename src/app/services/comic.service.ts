import { Injectable } from '@angular/core';
import { BaseService } from './base.service';
import { HttpClient } from '@angular/common/http';
import { Comic, ComicItem } from '../core/interfaces';
import { Storage } from '../core/storage';

@Injectable()
export class ComicService extends BaseService {

  private key_favs_comic:string = "keyfavscomics";
  comicsFavs : Array<Comic> = [];
  comicItemSelected:ComicItem;

  constructor(http:HttpClient) {
    super(http);
    this.getData();
  }

  getComicByUrl(url:string){
    return this.get(url+"?");
  }

  getData(){
    let data = Storage.getAll(this.key_favs_comic);
    if(data.comicFav){
      this.comicsFavs = data.comicFav;
    }
  }

  addComic(comic:Comic){
    this.comicsFavs.unshift(comic);
    Storage.setAll(this.key_favs_comic, {'comicFav': this.comicsFavs});
  }

  deleteComic(index:number){
    this.comicsFavs.splice(index, 1);
    Storage.setAll(this.key_favs_comic, {'comicFav': this.comicsFavs});
  }
}
