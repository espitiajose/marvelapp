export interface Pagination{
    offset: number,
    limit: number,
    total: number,
    count: number
}

export interface Thumbnail{
    path:string,
    extension:string
}

export interface ComicItem{
    resourceURI:string,
    name:string
}

export interface ComicItems{
    available: number,
    collectionURI: string,
    items:Array<ComicItem>
}
export interface Character{
    id:number,
    name:string,
    description:string,
    modified:string,
    thumbnail: Thumbnail,
    comics:ComicItems
}

export interface Price{
    type:string,
    price:number
}

export interface Comic{
    id:number,
    title: string,
    description:string,
    thumbnail: Thumbnail,
    prices: Array<Price>,
    resourceURI:string
}