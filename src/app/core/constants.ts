import { environment } from '../../environments/environment'


/**
 * Marvel apiKey key.
 * @type {[type]}
 */
const apikey = '6fc9b9b7fd88ad43ff7807bbb6bc0d30'; 

/**
 * Marvel ts key.
 * @type {[type]}
 */
const ts = '1';


/**
 * Marvel hash key.
 * @type {[type]}
 */
const hash = 'bf7490ddfc96510d0145c52fa89a17d2';

/**
 * Marvel hash key.
 * @type {[type]}
 */
export const marvelParams = '&ts='+ts+"&apikey="+apikey+"&hash="+hash;

/**
 * Path URL del APIRestfull.
 * @type {[type]}
 */
export const API_URL = environment.apiUrl;