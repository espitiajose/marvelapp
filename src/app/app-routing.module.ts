import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LandingComponent } from './system-pages/landing/landing.component';
import { NotFoundComponent } from './system-pages/not-found/not-found.component';
import { PAGES_ROUTES } from './pages/pages.router';


const routes: Routes = [
  { path: 'landing', component: LandingComponent },
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { useHash: true, onSameUrlNavigation: 'reload' } ),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
