import { Component, OnInit, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { Comic } from 'src/app/core/interfaces';
import { ComicService } from '../../services/comic.service';
import { ComicItem } from '../../core/interfaces';
import { Router } from '@angular/router';
declare var $:any;

@Component({
  selector: 'comic-details',
  templateUrl: './comic-details.component.html',
  styleUrls: ['./comic-details.component.css']
})
export class ComicDetailsComponent implements OnInit, OnChanges {

  @Output() event = new EventEmitter();
  @Output() close = new EventEmitter();
  comic:Comic;
  added:boolean = false;
  price: number;
  

  constructor(private _cS:ComicService,
              private _r: Router) { }

  ngOnInit() {
    $('[role="dialog"]').appendTo('body');
    $('#modalComicDetail').on('shown.bs.modal', (e) => {
      if(this._cS.comicItemSelected){
        this._cS.getComicByUrl(this._cS.comicItemSelected.resourceURI).subscribe((res:any)=>{
          this.comic = res.data.results[0];
          this.added = this.checkFav();
          this.price = this.getPrice();
        });
      }
    });
    $('#modalComicDetail').on('hidden.bs.modal', (e) => {
      this.close.emit();
      this._cS.comicItemSelected = undefined;
      this.comic = undefined;
    });

  }

  ngOnChanges(){
    
  }

  actionAdd(){
    if(!this.added){
      this._cS.addComic(this.comic);
      this.added = this.checkFav();
    } 
  }
  

  private checkFav(){
    for (const c of this._cS.comicsFavs) {
      if(c.id == this.comic.id) return true;
    }
    return false;
  }

  private getPrice(){
    if(this.comic.prices[1])
      return this.comic.prices[1].price;
    else
      return this.comic.prices[0].price;
  }
}
