import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { Comic, Character, ComicItem } from '../../core/interfaces';
import { ComicService } from '../../services/comic.service';
declare var $:any;

@Component({
  selector: 'ramdom-comics',
  templateUrl: './ramdom-comics.component.html',
  styleUrls: ['./ramdom-comics.component.css']
})
export class RamdomComicsComponent implements OnInit, OnChanges {

  @Input() character: Character;  
  @Output() close = new EventEmitter();
  comicsSelected:Array<ComicItem> = [];
  comicsFavs:Array<ComicItem> = [];

  constructor(private _cS:ComicService) { }

  ngOnInit() {
    $('[role="dialog"]').appendTo('body');
    $('#modalRamdomComic').on('hidden.bs.modal', (e) => {
      this.close.emit();
    });
    
  }

  ngOnChanges(){
    this.initData();
  }

  initData(){
      for (const itemComic of this.character.comics.items) {
        for (const c of this._cS.comicsFavs) {
          if(c.resourceURI == itemComic.resourceURI) this.comicsFavs.push(itemComic);
        }
      }
      this.selectRandom();
  }




  checkFav(itemComic:ComicItem){
    for (const c of this._cS.comicsFavs) {
      if(c.resourceURI == itemComic.resourceURI) return true;
    }
    return false;
  }

  checkComics(itemComic:ComicItem){
    for (const c of this.comicsSelected) {
      if(c.resourceURI == itemComic.resourceURI) return true;
    }
    return false;
  }

  selectRandom(){
    this.comicsSelected = [];
    let mm = this.character.comics.items.length - this.comicsFavs.length;
    if(mm > 3) mm = 3;
    while(this.comicsSelected.length < mm){
      let indexComic = Math.floor(Math.random() * this.character.comics.items.length);
      let comic = this.character.comics.items[indexComic];
      if(!this.checkFav(comic) && !this.checkComics(comic)){
        this.comicsSelected.push(comic);
      }
    }
  }

  actionAdd(){
    this.comicsSelected.forEach(c => {
      this._cS.getComicByUrl(c.resourceURI).subscribe((res:any)=>{
        this._cS.addComic(res.data.results[0]);
      })
    });
  }

}
