import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Pagination } from '../../core/interfaces';

@Component({
  selector: 'pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.css']
})
export class PaginationComponent implements OnInit, OnChanges {

  @Input() pagination : Pagination;
  @Output() event = new EventEmitter();
  numberPages : number;
  currentPage:number;

  constructor() {}

  ngOnInit() {
    this.loadData();
  }

  ngOnChanges(){
    this.loadData();
  }

  loadData(){
    this.numberPages = this.pagination.total / this.pagination.limit;
    this.currentPage = (this.pagination.offset / 10) + 1;
  }

  action(page:number){
    if(page > 0 && page < this.numberPages && page != this.currentPage){
      this.event.emit((page * 10) - 10)
    }
  }

  getPages(){
    let items : Array<any> = [];
    let start;
    if(this.currentPage >= 3)
      start = this.currentPage - 3;
    else
      start = 0;
    
    if(this.numberPages < 5) start = this.numberPages - 5;
    for (let i = start; i < start + 5; i++) {
      const item = i+1;
      items.push(item);
    }
    return items;
  }

}
