import { Component, OnInit, HostListener, Input, Output, EventEmitter } from '@angular/core';
import { Character } from 'src/app/core/interfaces';
import { ComicItem } from '../../core/interfaces';

@Component({
  selector: 'character',
  templateUrl: './character.component.html',
  styleUrls: ['./character.component.css']
})
export class CharacterComponent implements OnInit {

  expandRelated:boolean = false;
  cardMin:boolean = false;
  cardMinShip:boolean = false;
  @Input() character:Character;
  @Output() eventComic = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.resize(window.innerWidth);
  }

  actionSelectComic(comicItem: ComicItem){
    this.eventComic.emit(comicItem);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.resize(event.target.innerWidth);
  }

  resize(width:number){
    this.cardMin = width < 579;
    this.cardMinShip = (width < 750 && width > 575) || width < 460;
  }

}
