import { NgModule } from '@angular/core';
import { CharacterComponent } from './character/character.component';
import { ComicComponent } from './comic/comic.component';
import { BrowserModule } from '@angular/platform-browser';
import { ComicDetailsComponent } from './comic-details/comic-details.component';
import { PaginationComponent } from './pagination/pagination.component';
import { RouterModule } from '@angular/router';
import { RamdomComicsComponent } from './ramdom-comics/ramdom-comics.component';
@NgModule({
    declarations: [          
      CharacterComponent,
      ComicComponent,
      ComicDetailsComponent,
      PaginationComponent,
      RamdomComicsComponent
    ],
    imports: [
      BrowserModule,
      RouterModule
    ],
    exports: [
        CharacterComponent,
        ComicComponent,
        ComicDetailsComponent,
        PaginationComponent,
        RamdomComicsComponent
    ]
  })
  export class CustomComponentsModule { }