import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Comic } from '../../core/interfaces';
declare var $:any;

@Component({
  selector: 'comic',
  templateUrl: './comic.component.html',
  styleUrls: ['./comic.component.css']
})
export class ComicComponent implements OnInit {

  @Input() comic: Comic;
  @Output() event = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  selecComic(){
    this.event.emit('select');
  }

  actionDelete($event:Event){
    $event.stopPropagation();
    this.event.emit('delete');
    return false;
  }

}
