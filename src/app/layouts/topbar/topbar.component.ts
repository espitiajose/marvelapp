import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {

  textSearch:string;

  constructor(private _r: Router) { }

  ngOnInit() {
  }


  actionSearch(){
    if(this.textSearch && this.textSearch.length > 0) this._r.navigate(["/app/search/"+this.textSearch]);
    else this._r.navigate(["/app/search"]);
  }

}
