import { Component, OnInit } from '@angular/core';
import { Comic, ComicItem } from '../../core/interfaces';
import { ComicService } from '../../services/comic.service';
declare var $:any;

@Component({
  selector: 'favsbar',
  templateUrl: './favsbar.component.html',
  styleUrls: ['./favsbar.component.css']
})
export class FavsbarComponent implements OnInit {


  constructor(public _cS:ComicService) { }

  ngOnInit() {
  }

  eventComic($event, index:number){
    switch($event){
      case 'select':
        this.selectComic(this._cS.comicsFavs[index])
        break;
      case 'delete':
        this._cS.deleteComic(index)
        break;
    }
  }

  selectComic(comic:Comic){
    let comicItem:ComicItem = {
        name: comic.title,
        resourceURI: comic.resourceURI
    };
    this._cS.comicItemSelected = comicItem;
    $('#modalComicDetail').modal('show');
  }

}
