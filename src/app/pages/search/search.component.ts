import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ActivationEnd } from '@angular/router';
import { map, filter } from 'rxjs/operators';
import { Pagination, Character, ComicItem } from '../../core/interfaces';
import { CharacterService } from '../../services/character.service';
import { ComicService } from '../../services/comic.service';
declare var $:any;

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  textSearch:string = '';
  pagination : Pagination;
  characters: Array<Character> = [];

  constructor(private _cS:CharacterService,
              private _comS:ComicService,
              private router: Router,
              private route:ActivatedRoute) { }

  ngOnInit() {
    this.textSearch = this.route.snapshot.paramMap.get("text");
    this._cS.getCharacters(0,'name', this.textSearch).subscribe((res:any)=>{
      this.pagination = res.data;
      this.characters = res.data.results;
    });
    this.getDataRoute().subscribe( paramMap => {
      this.textSearch = paramMap.get("text");
      this.search();
    });
    window.scrollTo(0,0);
  }

  search(){
    this._cS.getCharacters(this.pagination.offset, 'name', this.textSearch).subscribe((res:any)=>{
      this.pagination = res.data;
      this.characters = res.data.results;
    });
  }

  paginationEvent($event){
    this._cS.getCharacters($event).subscribe((res:any)=>{
       this.pagination = res.data;
       this.characters = res.data.results;
       window.scrollTo(0,0);
    });
  }

  getDataRoute () {
    return this.router.events.pipe(
      filter( event =>  event instanceof ActivationEnd ),
      filter( (event: ActivationEnd) => event.snapshot.firstChild === null ),
      map( (event: ActivationEnd) => event.snapshot.paramMap  )
    )    
  }

  actionComicSelected(comicItem:ComicItem){
    this._comS.comicItemSelected = comicItem;
    $('#modalComicDetail').modal('show');
  }

}
