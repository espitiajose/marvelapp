import { PagesComponent } from './pages.component';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { CharacterProfileComponent } from './character-profile/character-profile.component';


const pagesRoutes: Routes = [
    {
      path: 'app',
      component: PagesComponent,
      children: [
        { path: 'home', component: HomeComponent },
        { path: 'search/:text', component: SearchComponent },
        { path: 'search', component: SearchComponent },
        { path: 'character/:id', component: CharacterProfileComponent },
        { path: '', redirectTo: 'landing', pathMatch: 'full' },
      ]
    }
  ];
  export const PAGES_ROUTES = RouterModule.forChild( pagesRoutes );