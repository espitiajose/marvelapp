import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeComponent } from './home/home.component';
import { PAGES_ROUTES } from './pages.router';
import { PagesComponent } from './pages.component';
import { TopbarComponent } from '../layouts/topbar/topbar.component';
import { FavsbarComponent } from '../layouts/favsbar/favsbar.component';
import { FooterComponent } from '../layouts/footer/footer.component';
import { CustomComponentsModule } from '../custom-components/custom-components.module';
import { SearchComponent } from './search/search.component';
import { ServicesModule } from '../services/service.mdule';
import { HttpClientModule } from '@angular/common/http';
import { CharacterProfileComponent } from './character-profile/character-profile.component';

@NgModule({
    declarations: [
      PagesComponent,
      TopbarComponent,
      FavsbarComponent,
      HomeComponent,
      FooterComponent,
      SearchComponent,
      CharacterProfileComponent
    ],
    imports: [
      BrowserModule,
      FormsModule,
      ReactiveFormsModule,
      PAGES_ROUTES,
      CustomComponentsModule,
      ServicesModule,
      HttpClientModule
    ]
  })
  export class PagesModule { }