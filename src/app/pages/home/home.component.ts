import { Component, OnInit, OnDestroy } from '@angular/core';
import { CharacterService } from '../../services/character.service';
import { Pagination, Character, ComicItem } from '../../core/interfaces';
import { FavsbarComponent } from '../../layouts/favsbar/favsbar.component';
import { ComicService } from '../../services/comic.service';
declare var $:any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  pagination : Pagination;
  characters: Array<Character> = [];

  constructor(private _cS:CharacterService, public _comS:ComicService) { }

  ngOnInit() {
    this._cS.getCharacters().subscribe((res:any)=>{
      this.pagination = res.data;
      this.characters = res.data.results;
    });
  }


  paginationEvent($event){
    this._cS.getCharacters($event).subscribe((res:any)=>{
       this.pagination = res.data;
       this.characters = res.data.results;
       window.scrollTo(0,0);
    });
  }

  changeSortBy($event:string){
    let orderBy = undefined;
    if($event != 'Sort by'){
      let parts = $event.split(' ');
      orderBy = parts[0].toLowerCase();
      if(parts[1] == "(asc)") orderBy = "-"+orderBy;
    }
    this._cS.getCharacters(this.pagination.offset, orderBy).subscribe((res:any)=>{
      this.pagination = res.data;
      this.characters = res.data.results;
    });
  }

  actionComicSelected(comicItem:ComicItem){
    this._comS.comicItemSelected = comicItem;
    $('#modalComicDetail').modal('show');
  }


  

}
