import { Component, OnInit } from '@angular/core';
import { CharacterService } from '../../services/character.service';
import { Character, ComicItem } from '../../core/interfaces';
import { Router, ActivatedRoute, ActivationEnd } from '@angular/router';
import { filter, map } from 'rxjs/operators';
import { ComicService } from '../../services/comic.service';
declare var $:any;

@Component({
  selector: 'app-character-profile',
  templateUrl: './character-profile.component.html',
  styleUrls: ['./character-profile.component.css']
})
export class CharacterProfileComponent implements OnInit {

  character:Character;
  constructor(private _cS:CharacterService,
              private _comS:ComicService,
              private router: Router,
              private route:ActivatedRoute) { }

  ngOnInit() {
    this.search();
    this.getDataRoute().subscribe( paramMap => {
      this.search();
		});
  }

  actionComicSelected(comicItem:ComicItem){
    this._comS.comicItemSelected = comicItem;
    $('#modalComicDetail').modal('show');
  }

  actionComicsRamdom(){
    $('#modalRamdomComic').modal('show');
  }

  search(){
    this._cS.getCharacter(this.route.snapshot.paramMap.get("id")).subscribe((res:any)=>{
      this.character = res.data.results[0];
    });
  }

  getDataRoute () {
    return this.router.events.pipe(
      filter( event =>  event instanceof ActivationEnd ),
      filter( (event: ActivationEnd) => event.snapshot.firstChild === null ),
      map( (event: ActivationEnd) => event.snapshot.paramMap  )
    )    
  }

  ngOnDestroy() {
    $('#modalRamdomComic').remove();
  }

}
